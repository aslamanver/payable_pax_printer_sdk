package com.payable.pax.printerlib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.util.Log;

public class PayablePrinter {

    public static String TAG_PRINT = "PRINT_CLIENT";

    public static String PRINTER_RESPONSE = "com.payable.pax.printer.response";
    public static String ACTION_PRINTER = "action_printer";
    public static String ACTION_STATUS = "action_status";
    public static String ACTION_SUCCESS = "action_success";
    public static String ACTION_FAIL = "action_fail";

    private static String CLIENT_APP_NAME = "com.payable.pax.printclient";
    private static String CLIENT_APP_RECEIVER = "com.payable.pax.printer.print";

    public static int STATUS_SUCCESS = 0;
    public static int STATUS_BUSY = 1;
    public static int STATUS_OUT_OF_PAPER = 2;
    public static int STATUS_FORMAT_ERROR = 3;
    public static int STATUS_MALFUNCTIONS = 4;
    public static int STATUS_OVER_HEAT = 8;
    public static int STATUS_VOLTAGE_LOW = 9;
    public static int STATUS_UNFINISHED = 240;
    public static int STATUS_NO_FONT_LIBRARY = 252;
    public static int STATUS_DATA_LONG = 254;
    public static int STATUS_NO_CLIENT_APP = 999;
    public static int STATUS_BITMAP_NULL = 998;

    private BroadcastReceiver localBroadcastReceiver;
    private Context context;
    private int callerId = 0;

    public PayablePrinter(Context context, final PayablePrinterListener payablePrinterListener) {

        this.context = context;

        localBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getStringExtra(PayablePrinter.ACTION_PRINTER);
                int status = intent.getIntExtra(PayablePrinter.ACTION_STATUS, -1);

                payablePrinterListener.onPrinterResponse(action, status, callerId);

                Log.d(TAG_PRINT, String.format("Printer onReceive: action => %s | status => %s | callerId => %s", action, status, callerId));

            }
        };

        registerReceiver();
    }

    public static BitmapUtils getBitmapUtils() {
        return new BitmapUtils();
    }

    public static void printBitmap(Context context, Bitmap bitmap) {

        Intent intentSend = new Intent();
        intentSend.setAction(PayablePrinter.PRINTER_RESPONSE);
        intentSend.putExtra(PayablePrinter.ACTION_PRINTER, PayablePrinter.ACTION_FAIL);

        if (!appFound(context, CLIENT_APP_NAME)) {

            intentSend.putExtra(PayablePrinter.ACTION_STATUS, STATUS_NO_CLIENT_APP);
            context.sendBroadcast(intentSend);

            // Toast.makeText(context, "PAYable Printer Client App is not installed", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intentBS = new Intent();
        intentBS.setAction(CLIENT_APP_RECEIVER);

        if (bitmap == null) {

            intentSend.putExtra(PayablePrinter.ACTION_STATUS, STATUS_BITMAP_NULL);
            context.sendBroadcast(intentSend);

            // Toast.makeText(context, "Bitmap is null", Toast.LENGTH_LONG).show();
            return;
        }

        intentBS.putExtra("printImage", PayablePrinter.getBitmapUtils().convertBitmapToByteArray(bitmap));
        context.sendBroadcast(intentBS);
    }

    public void printBitmap(Context context, Bitmap bitmap, int callerId) {
        this.callerId = callerId;
        printBitmap(context, bitmap);
    }

    private static boolean appFound(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    private void registerReceiver() {

        IntentFilter filter = new IntentFilter();
        filter.addAction(PayablePrinter.PRINTER_RESPONSE);
        context.registerReceiver(localBroadcastReceiver, filter);

    }

    public void unregisterReceiver() {

        Log.d(PayablePrinter.TAG_PRINT, "unregisterReceiver");

        try {
            context.unregisterReceiver(localBroadcastReceiver);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

}
