package com.payable.pax.printerlib;

public interface PayablePrinterListener {
    void onPrinterResponse(String action, int status, int callerId);
}
